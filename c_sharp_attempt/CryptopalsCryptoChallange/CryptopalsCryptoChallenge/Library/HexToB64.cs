﻿using System.Text;

namespace CryptopalsCryptoChallenge.Library;

public class HexToB64
{
    public static byte[] HexStringToBytes(char[] hexS)
    {
        return hexS switch
        {
            [var a, var b, .. var tail] => [(byte)(Hexify(a) << 4 | Hexify(b)) , .. HexStringToBytes(tail)],
            [var a] => [(byte)(Hexify(a) << 4)],
            [] => []
        };
    }

    private static byte Hexify(char c)
    {
        return c switch
        {
            '0' => 0,
            '1' => 1,
            '2' => 2,
            '3' => 3,
            '4' => 4,
            '5' => 5,
            '6' => 6,
            '7' => 7,
            '8' => 8,
            '9' => 9,
            'a' => 10,
            'b' => 11,
            'c' => 12,
            'd' => 13,
            'e' => 14,
            'f' => 15,
            _ => throw new Exception()
        };
    }
    
    public static string HexStringToBase64(string hexS)
    {
        var x = HexStringToBytes(hexS.ToCharArray());
        var str = Encoding.UTF8.GetString(x);
        return Convert.ToBase64String(x);
    }
}