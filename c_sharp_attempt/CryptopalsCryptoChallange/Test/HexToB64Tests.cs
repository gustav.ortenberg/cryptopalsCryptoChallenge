using System.Text;
using CryptopalsCryptoChallenge.Library;

namespace Test;

public class Tests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void Hex_to_B64()
    {
        var hex = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
        var expected = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t";
        
        var result = HexToB64.HexStringToBase64(hex);

        Assert.That(result, Is.EqualTo(expected));
    }

    [Test]
    public void Test_HexStr_toByte_evenNoHexes()
    {
        byte[] expected = [255, 255];

        var result = HexToB64.HexStringToBytes("ffff".ToCharArray());
        
        Assert.That(result, Is.EqualTo(expected));
    }

    [Test]
    public void Test_HexStr_toByte_oddNoHexes()
    {
        byte[] expected = [255, 240];

        var result = HexToB64.HexStringToBytes("fff".ToCharArray());

        Assert.That(result, Is.EqualTo(expected));
    }
}