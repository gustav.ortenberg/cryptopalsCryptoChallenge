#include <iostream>
#include <string>
using namespace std;

unsigned char * hexDecoder(string);
char hexEncoder(unsigned char);

int main(int argc, char const *argv[])
{
	string in1 = "1c0111001f010100061a024b53535009181c";
	string in2 = "686974207468652062756c6c277320657965";
	string output = "";
	unsigned char * pt1 = hexDecoder(in1);
	unsigned char * pt2 = hexDecoder(in2);
	unsigned char * ptRes;
	ptRes = (unsigned char*) malloc(in1.size()/2+1);
	for(int i = 0; i < in1.size()/2;i++){
		*(ptRes+i) = *(pt1+i)^ *(pt2+i);
		output.push_back(hexEncoder((*(ptRes+i) & 0xF0) >> 4));
		output.push_back(hexEncoder(*(ptRes+i) & 0x0F));
	}
	cout << output << "\n";
	return 0;
}

unsigned char * hexDecoder(string input){
	if (input.size()%2)
		input.insert(0,1,'0');
	char * inPt = & input.front();
	unsigned char * hex;
	hex = (unsigned char*) malloc(input.size()/2+1);
	unsigned char tmp;
	int j = 0;
	while(inPt != &input.back()+1){
		tmp = 0x00;
		for(int i = 0; i < 2;i++){
			tmp = tmp << 4 | 0x0;
			switch(*inPt++){
				case '0' : tmp |= 0x0; break;
				case '1' : tmp |= 0x1; break;
				case '2' : tmp |= 0x2; break;
				case '3' : tmp |= 0x3; break;
				case '4' : tmp |= 0x4; break;
				case '5' : tmp |= 0x5; break;
				case '6' : tmp |= 0x6; break;
				case '7' : tmp |= 0x7; break;
				case '8' : tmp |= 0x8; break;
				case '9' : tmp |= 0x9; break;
				case 'a' : tmp |= 0xa; break;
				case 'b' : tmp |= 0xb; break;
				case 'c' : tmp |= 0xc; break;
				case 'd' : tmp |= 0xd; break;
				case 'e' : tmp |= 0xe; break;
				case 'f' : tmp |= 0xf; break;
			}
		}
		*(hex+j++) = tmp;
	}
	return hex;
}

char hexEncoder(unsigned char hex){
	switch (hex){
		case 0x00 : return '0';
		case 0x01 : return '1';
		case 0x02 : return '2';
		case 0x03 : return '3';
		case 0x04 : return '4';
		case 0x05 : return '5';
		case 0x06 : return '6';
		case 0x07 : return '7';
		case 0x08 : return '8';
		case 0x09 : return '9';
		case 0x0a : return 'a';
		case 0x0b : return 'b';
		case 0x0c : return 'c';
		case 0x0d : return 'd';
		case 0x0e : return 'e';
		case 0x0f : return 'f';
	}
}