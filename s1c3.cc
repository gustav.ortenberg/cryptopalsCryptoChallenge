#include <iostream>
#include <stdio.h>
#include <string.h>
#include <string>
#include <math.h>
using namespace std;

char hexToBin(char);
bool isChar(char);
int score(string);

int main(int argc, char const *argv[])
{
	string input = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736";
	char * hex = & input.front();
	int i = 0;
	for(;i < input.size()/2; i++){
		*(hex+i) = hexToBin(*(hex+2*i)) << 4 | hexToBin(*(hex+2*i+1));
	}
	while(i < input.size()){
		input.pop_back();
	}
	char * maxStr;
	int max = 0;
	char dec;
	char * decoded;
	decoded = (char *) malloc(input.size()+1);
	char xOr = 0xFF;
	for(int i = 0; i < pow(2,8); i++){
		xOr--;
		int j = 0;
		for(; j < input.size();j++){
			dec = xOr ^ *(hex+j);
			if(!isChar(dec))
				break;
			*(decoded+j) = dec;
		}
		if(j == input.size() && score(decoded) > max){
			cout << decoded << " : " << j << "\n";
			max = score(decoded);
			strcpy(maxStr, decoded);
		}
	}
	cout << maxStr << "\n";

	return 0;
}

char hexToBin(char hex){
	switch(hex){
		case '0' : return 0x0; 
		case '1' : return 0x1;
		case '2' : return 0x2; 
		case '3' : return 0x3; 
		case '4' : return 0x4; 
		case '5' : return 0x5; 
		case '6' : return 0x6; 
		case '7' : return 0x7; 
		case '8' : return 0x8; 
		case '9' : return 0x9; 
		case 'a' : return 0xa; 
		case 'b' : return 0xb; 
		case 'c' : return 0xc; 
		case 'd' : return 0xd; 
		case 'e' : return 0xe; 
		case 'f' : return 0xf; 
	}
}

bool isChar(char in){
	if(in < 0x5b && in > 0x40)
		return true;
	if(in < 0x7b && in > 0x5f)
		return true;
	if(in == 0x3f)
		return true;
	if(in == 0x3a || in == 0x3b)
		return true;
	if(in == 0x2e || in == 0x2c)
		return true;
	if(in < 0x2a && in > 0x1f)
		return true;

	return false;
}

int score(string in){
	int score = 0;
	for(char i : in){
		if(0x60 < i && i < 0x7b)
			score++;
		if(i == 'e' && i == 'E')
			score += 4;
		//cont, with more heat mapping...
	}

	return score;
}