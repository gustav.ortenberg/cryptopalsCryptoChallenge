#include <iostream>
#include <stdio.h>
#include <string.h>
#include <string>
#include <math.h>
#include <fstream>
using namespace std;

void hexConv(string, char *);
bool isChar(char);
int score(string);

int main(int argc, char const *argv[])
{
	if(argc == 2){
		char * maxStr, * decoded, * hex, dec, xOr;
		int max = 0;
		string input;
		maxStr = (char *) malloc(31);
		decoded = (char *) malloc(31);
		hex = (char *) malloc(31);
		ifstream fin(argv[1]);
		while(!fin.eof()){
			getline(fin, input);
			hexConv(input, hex);
			xOr = 0xff;
			for(int i = 0; i < pow(2,8); i++){
				for(int j = 0; j < 30; j++){
					dec = xOr ^ *(hex+j);
					if(!isChar(dec))
						*(decoded+j) = 0x7e;
					else
						*(decoded+j) = dec;
				}
				if(score(decoded) > max){
					max = score(decoded);
					strcpy(maxStr, decoded);
				}
				xOr--;
			}
		}
		cout << maxStr << "\n";
	}
	return 0;
}

int score(string in){
	int score = 0;
	for(char i : in){
		if(i == 0x7e)
			score -= 100;
		i = tolower(i);
		if(i == 'e')
			score += 13;
		if(i == 't')
			score += 9;
		if(i == 'a' || i == 'o')
			score += 8;
		if(i == 'i' || i == 'n')
			score += 7;
		if(i == 's' || i == 'h' || i == 'r')
			score += 6;
		if(i == 'd' || i == 'l')
			score += 4;
		if(i == 'c' || i == 'u')
			score += 3;
		if(i == 'm' || i == 'w' || i == 'f' || i == 'g' || i == 'y' || i == 'p')
			score += 2;
		if(i == 'b' || i == 'v' || i == 'k')
			score += 1;
	}
	return score;
}

bool isChar(char in){
	if(in < 0x5b && in > 0x40)
		return true;
	if(in < 0x7b && in > 0x5f)
		return true;
	if(in == 0x3f)
		return true;
	if(in == 0x3a || in == 0x3b)
		return true;
	if(in == 0x2e || in == 0x2c)
		return true;
	if(in < 0x2a && in > 0x1f)
		return true;
	if(in == 0x0A)
		return true;
	return false;
}

char hexToBin(char hex){
	switch(hex){
		case '0' : return 0x0; 
		case '1' : return 0x1;
		case '2' : return 0x2; 
		case '3' : return 0x3; 
		case '4' : return 0x4; 
		case '5' : return 0x5; 
		case '6' : return 0x6; 
		case '7' : return 0x7; 
		case '8' : return 0x8; 
		case '9' : return 0x9; 
		case 'a' : return 0xa; 
		case 'b' : return 0xb; 
		case 'c' : return 0xc; 
		case 'd' : return 0xd; 
		case 'e' : return 0xe; 
		case 'f' : return 0xf; 
	}
}

void hexConv(string hex, char * conv){
	for(int i = 0; i < hex.size()/2; i++){
		*(conv+i) = hexToBin(hex.at(2*i)) << 4 | hexToBin(hex.at(2*i+1));
	}
}