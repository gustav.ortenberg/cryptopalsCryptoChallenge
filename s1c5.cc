#include <string.h>
#include <iostream>
#include <fstream>
using namespace std;

char * xorEncryptToHex(char *, char *);

int main(int argc, char const *argv[])
{
	if(argc == 3){
		ifstream fin(argv[1]);
		string input = "";
		char c;
		while(fin.get(c)){
			input += c;
		}
		string XOR = argv[2];
		cout << xorEncryptToHex(& input.at(0), & XOR.at(0)) <<"\n";
	}
	return 0;
}

char charToHex(unsigned char c){
	switch (c){
		case 0x00 : return '0';
		case 0x01 : return '1';
		case 0x02 : return '2';
		case 0x03 : return '3';
		case 0x04 : return '4';
		case 0x05 : return '5';
		case 0x06 : return '6';
		case 0x07 : return '7';
		case 0x08 : return '8';
		case 0x09 : return '9';
		case 0x0a : return 'a';
		case 0x0b : return 'b';
		case 0x0c : return 'c';
		case 0x0d : return 'd';
		case 0x0e : return 'e';
		case 0x0f : return 'f';
	}
}

char * xorEncryptToHex(char * plain, char * XOR){
	//XOR encrypt
	unsigned char * encrypted;
	encrypted = (unsigned char *) malloc(strlen(plain)+1);
	for (int i = 0; i < strlen(plain); i+=strlen(XOR)){
		for (int j = 0; j < strlen(XOR); j++){
			if(i+j == strlen(plain))
				break;
			*(encrypted+i+j) = *(plain+i+j) ^ *(XOR+j);
		} 
	}
	//Convert to hex
	char * hex;
	hex = (char *) malloc(strlen(plain)*2+1);
	for (int i = 0; i < strlen(plain); i++){
		*(hex+2*i)   = charToHex((*(encrypted+i) & 0xF0)>>4);
		*(hex+2*i+1) = charToHex((*(encrypted+i) & 0x0F));
	}
	return hex;
}


