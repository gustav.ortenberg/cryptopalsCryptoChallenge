#include <iostream>
#include <string.h>
#include <fstream>
using namespace std;

int editDist(char * txt1, char * txt2){
	char diff;
	int sum;
	for (int i = 0; i < strlen(txt1); i++){
		diff = *(txt1+i) ^ *(txt2+i);
		for (int j = 0; j < 8; j++){
			if(diff % 2)
				sum++;
			diff = diff >> 1;
		}
	}
	return sum;
}

int* keySize(char * chiper){
	int * keysize = new int[3];
	float minedit[] = {1000,1000,1000};
	for(int i = 2; i < 41; i++){
		float dist = 0;
		for(int a = 0; a < 3;a++){
			for(int b = a+1; b < 4;b++){
				char * txt1;
				char * txt2;
				txt1 = (char *) malloc(i+1);
				txt2 = (char *) malloc(i+1);
				strncpy(txt1, chiper+a*i, i);
				strncpy(txt2, chiper+b*i, i);
				dist =+ editDist(txt1, txt2)/i;
			}
		}
		dist /= 6;
		for (int i = 0; i < 3; ++i){
			if(dist < minedit[i]){
				minedit[i] = dist;
				keysize[i] = i;
			}	
		}

		
	}
	return keysize;
}

void b64ToChar(char * b64){
	//b64 to 6bit pattern
	for(int i = 0; i < strlen(b64); i++){
		if(0x60 < *(b64+i))
			*(b64+i) -= 0x47;
		if(0x40 < *(b64+i))
			*(b64+i) -= 0x41;
		if(0x2f < *(b64+i))
			*(b64+i) += 0x4;
		if(0x2b == *(b64+i))
			*(b64+i) = 0x3e;
		if(0x2f == *(b64+i))
			*(b64+i) = 0x3f;	
	}
	//Convert 6bit to 8bit pattern
	char * chEnc;
	int len = strlen(b64)*2/3+1;
	if(strlen(b64) % 3)
		len++;
	chEnc = (char *) malloc(len);
	int i = 0;
	//Unrolling
	switch(strlen(b64) % 3){
		case 0 : break;
		case 1 : *chEnc = *b64; i++; break;
		case 2 : *chEnc = (*b64 & 0x3C) >> 2; 
		         *(chEnc+1) = ((*b64 & 0x03) << 6) | (*(b64+1));
		         i += 2; break;
	}
	//Rest
	for (; i < strlen(b64);i++){

	}
}

char* findXorKey(char * chiper, int keylen){
	char * key = (char *) malloc(keylen+1);
	char * plain = (char *) malloc(strlen(chiper)+1);
	//Insert good algo that bruteForces the best key (copy pasta)
	return key;
}


int main(int argc, char const *argv[]){
	if(argc == 2){
		//Extract txt form file
		ifstream fin(argv[1]);
		string input = "";
		char c;
		while(fin.get(c)){
			input += c;
		}
		//Convert form b64 to char
		b64ToChar(& input.at(0));
		//Find the best keylength
		int * keysize = keySize(& input.at(0));
	}

	return 0;
}

/*
1. let keysize 2-40 (check)
2. edit dist (check)
3. ∀ keysize find relative edit distance within the crypt (check)
4. Take the best keysize dists (2-3) and use for brute 
5. Split cipher into keysize len
6. For each block separate into chars 
7. Find the xor char for each chiper char
8. Gain key
*/
