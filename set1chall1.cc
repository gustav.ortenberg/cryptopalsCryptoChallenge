#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

char hexToBase64(unsigned char);

int main(int argc, char const *argv[])
{
	string input = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
	if (input.size()%2)
		input.insert(0,1,'0');
	char * inPt = & input.front();
	unsigned char * hex;
	hex = (unsigned char*) malloc(input.size()/2+1);
	unsigned char tmp;
	int j = 0;
	while(inPt != &input.back()+1){
		tmp = 0x00;
		for(int i = 0; i < 2;i++){
			tmp = tmp << 4 | 0x0;
			switch(*inPt++){
				case '0' : tmp |= 0x0; break;
				case '1' : tmp |= 0x1; break;
				case '2' : tmp |= 0x2; break;
				case '3' : tmp |= 0x3; break;
				case '4' : tmp |= 0x4; break;
				case '5' : tmp |= 0x5; break;
				case '6' : tmp |= 0x6; break;
				case '7' : tmp |= 0x7; break;
				case '8' : tmp |= 0x8; break;
				case '9' : tmp |= 0x9; break;
				case 'a' : tmp |= 0xa; break;
				case 'b' : tmp |= 0xb; break;
				case 'c' : tmp |= 0xc; break;
				case 'd' : tmp |= 0xd; break;
				case 'e' : tmp |= 0xe; break;
				case 'f' : tmp |= 0xf; break;
			}
		}
		*(hex+j++) = tmp;
	}
	string output = "";
	int state = 0;
	unsigned char tmp2;
	while(j--){
		cout << *(hex+j) << " : ";
		switch(state++){
			case 0 : tmp  = 0x3f & *(hex+j);
					 tmp2 = 0x03 & *(hex+j) >> 6; 
					 break;
			case 1 : tmp |= 0x3C & *(hex+j) << 2;
					 tmp2 = 0x0f & *(hex+j) >> 4; 
					 break;
			case 2 : tmp |= 0x30 & *(hex+j) << 4;
					 cout << hexToBase64(tmp) << " : ";
					 output.push_back(hexToBase64(tmp));
					 tmp = 0x3F & *(hex+j) >> 2; 
					 break;
		}
		cout << hexToBase64(tmp) << "\n";
		output.push_back(hexToBase64(tmp));
		tmp = tmp2;
		state %= 3;
	}
	char * otPt = &output.back();
	while(otPt != &output.front()){
		cout << *otPt--;
	}
	cout << *otPt <<"\n";

//AAAAAA BB|BBBB CCCC|CC DDDDD


	return 0;
}

char hexToBase64(unsigned char hex){
	if(hex < 26)
		return (char)hex+0x41;
	if(hex < 52)
		return (char)hex+0x47;
	if(hex < 62)
		return (char)hex-0x4;
	if(hex < 63)
		return (char)0x2b;
	if(hex < 64)
		return (char)0x2f;
	return (char)0x00;
}

/*base64
 0-25 = A-Z
26-51 = a-z
52-61 = 0-9
   62 = +
   63 = /
*/
